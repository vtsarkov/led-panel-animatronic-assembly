/****************************************************************************
 * Copyright (C) 2016 Vadim Tsarkov, Siddhant Modi                          *
 *                                                                          *
 * Description:                                                             *
 * Code created for MSE 352 - Digital Logic and MCUs                        *
 * final project.                                                           *
 *                                                                          *
 * Requirements:                                                            *
 * Code is designed for Tiva C-series board with ARM Cortex M4 CPU.         *
 * Code Composer Studio (TI) along with TivaWare library is required        *
 * to compile this code. Pinout diagram is not provided.                    *
 *                                                                          *
 * Available at:                                                            *
 * https://gitlab.com/vtsarkov/led-panel-animatronic-assembly               *
 ****************************************************************************/

#include <stdint.h>
#include <stdbool.h>
#include "inc/tm4c123gh6pm.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "driverlib/debug.h"
#include "driverlib/sysctl.h"
#include "driverlib/adc.h"
#include "driverlib/gpio.h"
#include "driverlib/uart.h"
#include "utils/uartstdio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/timer.h"
#include "driverlib/pwm.h"
#include "driverlib/rom.h"
#include "driverlib/systick.h"
#include "ascii_char_bitmap.h"

#define PWM_FREQUENCY 55                            //PWM module frequency [Hz]
#define MAX_PWM_ADJUST 14000                        //PWM signal control constants
#define MIN_PWM_ADJUST 2650
#define STAGE_1_PWM_INCREMENT 1
#define STAGE_2_PWM_INCREMENT 3
#define ADC_MAP_MIDPOINT 2000
#define STAGE_1_INCREMENT_BOUNDARY_LOW 1750         //ADC value threshold for
#define STAGE_2_INCREMENT_BOUNDARY_LOW 1250         //servo speed adjustment
#define STAGE_1_INCREMENT_BOUNDARY_HIGH 2250
#define STAGE_2_INCREMENT_BOUNDARY_HIGH 2750
#define rowData                 0x00000004          // GPIO pin 2
#define rowShiftClock           0x00000008          // GPIO pin 3
#define rowStorageClock         0x00000010          // GPIO pin 4
#define columnData              0x00000020          // GPIO pin 5
#define columnShiftClock        0x00000040          // GPIO pin 6
#define columnStorageClock      0x00000080          // GPIO pin 7

//global variables
volatile uint32_t ui32Load;
volatile uint32_t pitchPwmAdjust;
volatile uint32_t yawPwmAdjust;
volatile uint32_t neckPwmAdjust;
uint32_t pitchAdcMap;
uint32_t yawAdcMap;
uint32_t buttonState = 1;
uint32_t currentLetter = 0;

//function prototypes
void send_SerialColumnData(uint32_t);
uint32_t strlen(char*);
void btnIntHandler(void);
void initADC(void);
void initUART(void);
void initGPIOF(void);
void initTimers(void);
void initPWM(void);
void initPE4Interrupt(void)

int main(void){
    //initialize peripherals
    initADC();
    initUART();
    initGPIOF();
    initTimers();
    initPWM();
    initPE4Interrupt();

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, rowData|rowShiftClock|rowStorageClock|columnData|columnShiftClock|columnStorageClock);

    uint32_t displayMemory [] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    uint8_t i, k, l, shiftRow, scroll, shift_step = 1, stringLength, index, charWidth;
    uint32_t speed = 1, temp; //random number

    char message[] = " !#$%&()*+,-.0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[^_abcdefghijklmnopqrstuvwxyz{|}~";
    stringLength = strlen(message);

    while (1) {
        for (k = 0; k < stringLength; k++) {
            index = message[k];
            currentLetter = index;
            UARTprintf("Currently Printing: %c\r", index);            
            charWidth = characterWidth[index-32];
            for (scroll = 0; scroll < (charWidth/shift_step); scroll++) {
                for(shiftRow = 0; shiftRow < 16; shiftRow++) {
                    temp = CharData[index-32][shiftRow];
                    displayMemory[shiftRow] = (displayMemory[shiftRow] << shift_step) | (temp >> ((charWidth-shift_step)-scroll*shift_step));
                } //shiftRow loop ends
                for (l = 0; l < speed; l++) {
                    for(i = 0; i < 16; i++) {
                        if (l == 0)
                            send_SerialColumnData(displayMemory[i]);
                        if (i == 0)
                            GPIOPinWrite(GPIO_PORTA_BASE, rowData, 0xFF);
                        else
                            GPIOPinWrite(GPIO_PORTA_BASE, rowData, 0x00);

                        GPIOPinWrite(GPIO_PORTA_BASE, rowShiftClock, 0xFF);
                        GPIOPinWrite(GPIO_PORTA_BASE, rowShiftClock, 0x00);
                        GPIOPinWrite(GPIO_PORTA_BASE, rowStorageClock, 0xFF);
                        GPIOPinWrite(GPIO_PORTA_BASE, rowStorageClock, 0x00);
                        SysCtlDelay(20000);
                    } //i loop ends
                }// l loop ends
            }//scroll loop ends
        } //k loop ends
    }
}

void send_SerialColumnData(uint32_t colData) {
    uint32_t mask = 0x80000000;
    uint8_t t = 0;
    for (; t<32; t++) {
        if(mask&colData)
            GPIOPinWrite(GPIO_PORTA_BASE, columnData, 0xFF);
        else
            GPIOPinWrite(GPIO_PORTA_BASE, columnData, 0x00);

        mask >>= 1;

        GPIOPinWrite(GPIO_PORTA_BASE, columnShiftClock, 0xFF);
        GPIOPinWrite(GPIO_PORTA_BASE, columnShiftClock, 0x00);
    }

    GPIOPinWrite(GPIO_PORTA_BASE, columnStorageClock, 0xFF);
    GPIOPinWrite(GPIO_PORTA_BASE, columnStorageClock, 0x00);
}

//returns length of input string as int
uint32_t strlen(char* myString) {
    uint32_t i = 0;
    while (*myString != '\0') {
        i++;
        myString++;
    }
    return i;
}

void btnIntHandler(void){
    GPIOIntDisable(GPIO_PORTF_BASE, GPIO_PIN_4);        // Disable interrupt for PF4 (in case it was enabled)
    GPIOIntClear(GPIO_PORTF_BASE, GPIO_PIN_4);          // Clear pending interrupts for PF4

    if(buttonState == 1){
        buttonState = 2;
        neckPwmAdjust = MAX_PWM_ADJUST;
        ROM_PWMPulseWidthSet(PWM1_BASE, PWM_OUT_3, neckPwmAdjust * ui32Load / 100000);
    } else if(buttonState == 2) {
        buttonState = 3;
        neckPwmAdjust = 8300;
        ROM_PWMPulseWidthSet(PWM1_BASE, PWM_OUT_3, neckPwmAdjust * ui32Load / 100000);
    }else if(buttonState == 3){
        buttonState = 4;
        neckPwmAdjust = MIN_PWM_ADJUST;
        ROM_PWMPulseWidthSet(PWM1_BASE, PWM_OUT_3, neckPwmAdjust * ui32Load / 100000);
    }else if(buttonState == 4){
        buttonState = 1;
        neckPwmAdjust = 8300;
        ROM_PWMPulseWidthSet(PWM1_BASE, PWM_OUT_3, neckPwmAdjust * ui32Load / 100000);
    }

    SysCtlDelay(40000);

    GPIOIntEnable(GPIO_PORTF_BASE, GPIO_PIN_4);         // Enable interrupt for PF4

}

void Timer0AIntHandler(void)
{
    // disable and clear the timer interrupt
    TimerIntDisable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
    TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

    //clear ADC interrupts and trigget adc processors
    ADCIntClear(ADC0_BASE, 3);
    ADCProcessorTrigger(ADC0_BASE, 3);
    ADCIntClear(ADC1_BASE, 3);
    ADCProcessorTrigger(ADC1_BASE, 3);

    // wait untill ADC processing is complete
    while(!ADCIntStatus(ADC0_BASE, 3, false) || !ADCIntStatus(ADC1_BASE, 3, false))
    {
    }

    //obtain analog -> digital maps
    ADCSequenceDataGet(ADC0_BASE, 3, &yawAdcMap);
    ADCSequenceDataGet(ADC1_BASE, 3, &pitchAdcMap);

    //based on the obtained value, assign pwm increments
    if(pitchAdcMap < STAGE_1_INCREMENT_BOUNDARY_LOW && pitchAdcMap > STAGE_2_INCREMENT_BOUNDARY_LOW) {
        pitchPwmAdjust -= STAGE_1_PWM_INCREMENT;
    }else if(pitchAdcMap < STAGE_2_INCREMENT_BOUNDARY_LOW) {
        pitchPwmAdjust -= STAGE_2_PWM_INCREMENT;
    }else if(pitchAdcMap > STAGE_1_INCREMENT_BOUNDARY_HIGH && pitchAdcMap < STAGE_2_INCREMENT_BOUNDARY_HIGH) {
        pitchPwmAdjust += STAGE_1_PWM_INCREMENT;
    }else if(pitchAdcMap > STAGE_2_INCREMENT_BOUNDARY_HIGH){
        pitchPwmAdjust += STAGE_2_PWM_INCREMENT;
    }

    if(yawAdcMap < STAGE_1_INCREMENT_BOUNDARY_LOW && yawAdcMap > STAGE_2_INCREMENT_BOUNDARY_LOW) {
            yawPwmAdjust -= STAGE_1_PWM_INCREMENT;
        }else if(yawAdcMap < STAGE_2_INCREMENT_BOUNDARY_LOW) {
            yawPwmAdjust -= STAGE_2_PWM_INCREMENT;
        }else if(yawAdcMap > STAGE_1_INCREMENT_BOUNDARY_HIGH && yawAdcMap < STAGE_2_INCREMENT_BOUNDARY_HIGH) {
            yawPwmAdjust += STAGE_1_PWM_INCREMENT;
        }else if(yawAdcMap > STAGE_2_INCREMENT_BOUNDARY_HIGH){
            yawPwmAdjust += STAGE_2_PWM_INCREMENT;
    }

    // PWM extreme boundaries
    if(pitchPwmAdjust < MIN_PWM_ADJUST) pitchPwmAdjust = MIN_PWM_ADJUST;
    if(pitchPwmAdjust > MAX_PWM_ADJUST) pitchPwmAdjust = MAX_PWM_ADJUST;
    if(yawPwmAdjust < MIN_PWM_ADJUST) yawPwmAdjust = MIN_PWM_ADJUST;
    if(yawPwmAdjust > MAX_PWM_ADJUST) yawPwmAdjust = MAX_PWM_ADJUST;

    //modify pulse width
    ROM_PWMPulseWidthSet(PWM1_BASE, PWM_OUT_0, yawPwmAdjust * ui32Load / 100000);
    ROM_PWMPulseWidthSet(PWM1_BASE, PWM_OUT_1, pitchPwmAdjust * ui32Load / 100000);

    //re-enable timer interrupt
    TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
}

void UARTIntHandler(void)
{
    uint32_t ui32Status;

    ui32Status = UARTIntStatus(UART0_BASE, true); //get interrupt status

    UARTIntClear(UART0_BASE, ui32Status); //clear the asserted interrupts

    while(UARTCharsAvail(UART0_BASE)) //loop while there are chars
    {
        //read available char from UART
        int ch = UARTCharGetNonBlocking(UART0_BASE);

        //if char is 's' - trigger pusle width/duty cycle calculations
        if(ch == 's'){

            int count;
            for(count = 0; count < 20; count++){
                UARTprintf("\n");
            }

            //PWM duty cycle and ADC voltage calculations
            uint32_t pitchPulseWidth;
            uint32_t pitchDutyCycle;
            uint32_t pitchVoltage;
            pitchPulseWidth = 10*pitchPwmAdjust / PWM_FREQUENCY;
            pitchDutyCycle = (pitchPulseWidth*100)/(1000000/55);
            pitchVoltage = pitchAdcMap*3300/4096;
            uint32_t yawPulseWidth;
            uint32_t yawDutyCycle;
            uint32_t yawVoltage;
            yawPulseWidth = 10*yawPwmAdjust / PWM_FREQUENCY;
            yawDutyCycle = (yawPulseWidth*100)/(1000000/55);
            yawVoltage = yawAdcMap*3300/4096;
            uint32_t neckPulseWidth;
            uint32_t neckDutyCycle;
            neckPulseWidth = 10*neckPwmAdjust / PWM_FREQUENCY;
            neckDutyCycle = (neckPulseWidth*100)/(1000000/55);


            // terminal UART screen
            UARTprintf("Servo Control:\n");
            UARTprintf("Pitch Servo: Pusle Width = %5dus \t Duty Cycle = %d percent\r\n", pitchPulseWidth, pitchDutyCycle);
            UARTprintf("Yaw   Servo: Pusle Width = %5dus \t Duty Cycle = %d percent\r\n", yawPulseWidth, yawDutyCycle);
            UARTprintf("Neck  Servo: Pusle Width = %5dus \t Duty Cycle = %d percent\r\n", neckPulseWidth, neckDutyCycle);
            UARTprintf("ADC Status:\n");
            UARTprintf("Pitch Joystick Voltage: %d mV\t Yaw Joystick Voltage: %d mV\r\n", pitchVoltage, yawVoltage);

            continue;
        }
    }
}

void initADC(void){
    SysCtlClockSet(SYSCTL_SYSDIV_5|SYSCTL_USE_PLL|SYSCTL_OSC_MAIN|SYSCTL_XTAL_16MHZ);

    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);        //enable adc0 peripheral
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC1);        //enable adc1 peripheral
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);    //enable GPIOE peripheral

    GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_3);    //set PIN 3 of GPIO port E to type ADC
    GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_2);    //set PIN 2 of GPIO port E to type ADC

    //ADC module initialization, sequencers 3 are used for both modules
    ADCSequenceDisable(ADC0_BASE, 3);
    ADCSequenceConfigure(ADC0_BASE, 3, ADC_TRIGGER_PROCESSOR, 0);
    ADCSequenceStepConfigure(ADC0_BASE, 3, 0, ADC_CTL_CH0 | ADC_CTL_IE | ADC_CTL_END);
    ADCSequenceEnable(ADC0_BASE, 3);

    ADCSequenceDisable(ADC1_BASE, 3);
    ADCSequenceConfigure(ADC1_BASE, 3, ADC_TRIGGER_PROCESSOR, 1);
    ADCSequenceStepConfigure(ADC1_BASE, 3, 0, ADC_CTL_CH1 | ADC_CTL_IE | ADC_CTL_END);
    ADCSequenceEnable(ADC1_BASE, 3);

    ADCIntClear(ADC0_BASE, 3);                        //clear interrupt flag before sampling begins
    ADCIntClear(ADC1_BASE, 3);
}

void initUART(void){
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
    UARTStdioConfig(0, 115200, SysCtlClockGet());

    UARTConfigSetExpClk(UART0_BASE, SysCtlClockGet(), 115200,
        (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE));

    IntEnable(INT_UART0); //enable the UART interrupt
    UARTIntEnable(UART0_BASE, UART_INT_RX | UART_INT_RT); //only enable RX and TX interrupts
}

void initGPIOF(void){
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);
}

void initTimers(void){

    //Timer 0 for ADC sampling
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
    TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC);

    uint32_t AdcSamplingPeriod;
    AdcSamplingPeriod = SysCtlClockGet()/2000;
    TimerLoadSet(TIMER0_BASE, TIMER_A, AdcSamplingPeriod -1);

    IntEnable(INT_TIMER0A);
    TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

    TimerEnable(TIMER0_BASE, TIMER_A);

    IntMasterEnable();
}

void initPWM(void){
    volatile uint32_t ui32PWMClock;
    pitchPwmAdjust = 8300;
    yawPwmAdjust = 8300;
    neckPwmAdjust = 8300;

    ROM_SysCtlPWMClockSet(SYSCTL_PWMDIV_64);

    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM1);
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);

    ROM_GPIOPinTypePWM(GPIO_PORTD_BASE, GPIO_PIN_0|GPIO_PIN_1);
    ROM_GPIOPinTypePWM(GPIO_PORTE_BASE, GPIO_PIN_5);

    ROM_GPIOPinConfigure(GPIO_PD0_M1PWM0);
    ROM_GPIOPinConfigure(GPIO_PD1_M1PWM1);
    ROM_GPIOPinConfigure(GPIO_PE5_M1PWM3);

    ui32PWMClock = SysCtlClockGet() / 64;
    ui32Load = (ui32PWMClock / PWM_FREQUENCY) - 1;
    PWMGenConfigure(PWM1_BASE, PWM_GEN_0, PWM_GEN_MODE_DOWN);
    PWMGenPeriodSet(PWM1_BASE, PWM_GEN_0, ui32Load);

    PWMGenConfigure(PWM1_BASE, PWM_GEN_1, PWM_GEN_MODE_DOWN);
    PWMGenPeriodSet(PWM1_BASE, PWM_GEN_1, ui32Load);

    ROM_PWMPulseWidthSet(PWM1_BASE, PWM_OUT_0, yawPwmAdjust * ui32Load / 100000);
    ROM_PWMPulseWidthSet(PWM1_BASE, PWM_OUT_1, pitchPwmAdjust * ui32Load / 100000);
    ROM_PWMPulseWidthSet(PWM1_BASE, PWM_OUT_3, neckPwmAdjust * ui32Load / 100000);

    ROM_PWMOutputState(PWM1_BASE, PWM_OUT_0_BIT | PWM_OUT_1_BIT | PWM_OUT_3_BIT, true);

    ROM_PWMGenEnable(PWM1_BASE, PWM_GEN_0);
    ROM_PWMGenEnable(PWM1_BASE, PWM_GEN_1);
}

void initPE4Interrupt(void){
    // Pin F4 setup
    GPIOPinTypeGPIOInput(GPIO_PORTF_BASE, GPIO_PIN_4);  // Init PF4 as input
    GPIOPadConfigSet(GPIO_PORTF_BASE, GPIO_PIN_4,
        GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);         // Enable weak pullup resistor for PF4

    // Interrupt setup
    GPIOIntDisable(GPIO_PORTF_BASE, GPIO_PIN_4);        // Disable interrupt for PF4 (in case it was enabled)
    GPIOIntClear(GPIO_PORTF_BASE, GPIO_PIN_4);          // Clear pending interrupts for PF4
    GPIOIntRegister(GPIO_PORTF_BASE, *btnIntHandler);
    GPIOIntTypeSet(GPIO_PORTF_BASE, GPIO_PIN_4,
        GPIO_BOTH_EDGES);                                 // Configure PF4 for falling edge trigger
    GPIOIntEnable(GPIO_PORTF_BASE, GPIO_PIN_4);         // Enable interrupt for PF4
}
