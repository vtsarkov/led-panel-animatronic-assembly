## Final Project for Digital Logic and MCUs course @ Simon Fraser University

### Team Members:
* Vadim Tsarkov [GitLab Projects](https://gitlab.com/u/vtsarkov)
* Siddhant Modi [GitLab Projects](https://gitlab.com/u/siddhantmodi)

### Project Description
The project consists of two parts: 
1. A 512-LED display along with the drive and control circuitry allowing control via just 6 GPIO pins.
2. An animatronic assembly of a bird, designed using SolidWorks, build using laser-cut parts, servo motors and tons of epoxy, and controlled using PS3 joysticks.

Together, they are controlled by a Tiva C-series board, equipped with an ARM Cortex M4 processor. The code simultaneously controls the scrolling text on the display as well as the motion of the bird.

### Code Features:
* MCU peripheral initialization and setup
* Timers and PWM generation
* ADC
* Interruts
* Digital Logic

### Additional Information:
[Video](https://www.youtube.com/watch?v=KTiETS3Y2Ns&feature=youtu.be)

[Circuit Schematic](https://www.dropbox.com/s/lo9wnkdk5ixmpy3/Schematic_v2.pdf?dl=0)

### Images:
Bird SolidWorks Model
![Bird Model](http://i.imgur.com/NtfvrQa.png?1)
